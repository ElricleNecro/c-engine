#include "cengine/events.h"

bool update_events_t(events_t *evt) {
	int pending = SDL_PollEvent(&evt->evt);

	// Getting the focused windowID:
	evt->winID = evt->evt.window.windowID;

	evt->wx = 0;
	evt->wy = 0;
	evt->dx = 0;
	evt->dy = 0;
	evt->resized = false;
	evt->event_used = false;

	// Processing the event:
	switch (evt->evt.type) {
		case SDL_WINDOWEVENT:
			if (evt->evt.window.event == SDL_WINDOWEVENT_CLOSE) {
				evt->close_window = true;
			} else if (evt->evt.window.event == SDL_WINDOWEVENT_SHOWN) {
			} else if (evt->evt.window.event == SDL_WINDOWEVENT_HIDDEN) {
			} else if (evt->evt.window.event == SDL_WINDOWEVENT_EXPOSED) {
			} else if (evt->evt.window.event == SDL_WINDOWEVENT_MOVED) {
			} else if (evt->evt.window.event == SDL_WINDOWEVENT_RESIZED) {
				evt->resized = true;
				evt->width = evt->evt.window.data1;
				evt->height = evt->evt.window.data2;
			} else if (evt->evt.window.event == SDL_WINDOWEVENT_SIZE_CHANGED) {
			} else if (evt->evt.window.event == SDL_WINDOWEVENT_MINIMIZED) {
				evt->minimized = true;
			} else if (evt->evt.window.event == SDL_WINDOWEVENT_MAXIMIZED) {
				evt->minimized = false;
			} else if (evt->evt.window.event == SDL_WINDOWEVENT_RESTORED) {
				evt->minimized = false;
			} else if (evt->evt.window.event == SDL_WINDOWEVENT_ENTER) {
			} else if (evt->evt.window.event == SDL_WINDOWEVENT_LEAVE) {
			} else if (evt->evt.window.event == SDL_WINDOWEVENT_FOCUS_GAINED) {
			} else if (evt->evt.window.event == SDL_WINDOWEVENT_FOCUS_LOST) {
			} else if (evt->evt.window.event == SDL_WINDOWEVENT_TAKE_FOCUS) {
			} else if (evt->evt.window.event == SDL_WINDOWEVENT_HIT_TEST) {
			}
			break;

		case SDL_KEYDOWN:
			evt->last_register = evt->evt.key.keysym.scancode;
			evt->kPress[evt->evt.key.keysym.scancode] = true;
			break;

		case SDL_KEYUP:
			if (evt->last_register == evt->evt.key.keysym.scancode)
				evt->last_register = SDL_NUM_SCANCODES;

			evt->kPress[evt->evt.key.keysym.scancode] = false;
			break;

		case SDL_MOUSEBUTTONDOWN:
			evt->kMouse[evt->evt.button.button] = true;
			break;

		case SDL_MOUSEBUTTONUP:
			evt->kMouse[evt->evt.button.button] = false;
			break;

		case SDL_MOUSEMOTION:
			evt->dx = evt->evt.motion.xrel;
			evt->dy = evt->evt.motion.yrel;
			break;

		case SDL_MOUSEWHEEL:
			evt->wx = evt->evt.wheel.x;
			evt->wy = evt->evt.wheel.y;
			break;

		case SDL_QUIT:
			evt->quit = true;
			break;
	}

	// Getting cursor position:
	evt->x = evt->evt.motion.x;
	evt->y = evt->evt.motion.y;

	return ! (pending == 0);
}
