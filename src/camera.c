#include "cengine/camera.h"

camera_t new_camera_perspective(float fov, float aspect_ratio, vec3 eye, vec3 center, vec3 up) {
	camera_t cam = {
		.aspect_ratio = aspect_ratio,
		.position = {eye[0], eye[1], eye[2]},

		.kind = PERSPECTIVE,

		.fov = fov,
		.near = -1.f,
		.far = 1.f,

		.pitch = 0.f,
		.yaw = 0.f,
		.roll = 0.f,
	};

	glm_mat4_identity(cam.view);
	glm_perspective(cam.fov, cam.aspect_ratio, cam.near, cam.far, cam.projection);
	glm_lookat(eye, center, up, cam.view);

	// cam = update_view_camera(cam);
	glm_mat4_mul(cam.projection, cam.view, cam.view_projection);

	return cam;
}

camera_t new_camera_orthographic(float aspect_ratio, bool rotatable) {
	camera_t cam = {
		.aspect_ratio = aspect_ratio,
		.position = {0.f, 0.f, 5.f},

		.kind = ORTHOGRAPHIC,

		.rotatable = rotatable,
		.zoom_level = 1.0f,
	};

	glm_mat4_identity(cam.view);
	setup_ortho_camera(cam.aspect_ratio, cam.zoom_level, cam.projection);

	cam = update_view_camera(cam);

	return cam;
}

camera_t update_view_camera(camera_t cam) {
	switch(cam.kind) {
		case PERSPECTIVE: {
			mat4 trans, res, rotation;

			glm_mat4_identity(trans);
			glm_translate(trans, cam.position);
			glm_euler((vec3){-cam.pitch, -cam.yaw, cam.roll}, rotation);
			glm_mat4_mul(trans, rotation, res);
			glm_mat4_inv(res, cam.view);

			break;
		}

		case ORTHOGRAPHIC: {
			glm_mat4_identity(cam.view);
			glm_translate(cam.view, cam.position);
			break;
		}
	}

	glm_mat4_mul(cam.projection, cam.view, cam.view_projection);

	return cam;
}

camera_t update_aspect_ratio(camera_t cam, const float aspect_ratio) {
	cam.aspect_ratio = aspect_ratio;

	switch(cam.kind) {
		case PERSPECTIVE:
			glm_perspective_resize(cam.aspect_ratio, cam.projection);
			glm_mat4_mul(cam.projection, cam.view, cam.view_projection);

			break;

		case ORTHOGRAPHIC:
			setup_ortho_camera(aspect_ratio, cam.zoom_level, cam.projection);
			break;
	}

	return cam;
}
