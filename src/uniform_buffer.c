#include "cengine/uniform_buffer.h"

#include <GL/glew.h>
#include <GL/gl.h>

uniform_buffer new_uniform_buffer(const size_t size, const uint32_t binding) {
	uniform_buffer id = {0};

	glCreateBuffers(1, &id.id);
	glNamedBufferData(id.id, size, NULL, GL_DYNAMIC_DRAW);
	glBindBufferBase(GL_UNIFORM_BUFFER, binding, id.id);

	return id;
}

void delete_uniform_buffer(const uniform_buffer id) {
	glDeleteBuffers(1, &id.id);
}

void set_data_uniform_buffer(const uniform_buffer id, const void *data, const size_t size, const size_t offset) {
	glNamedBufferSubData(id.id, offset, size, data);
}
