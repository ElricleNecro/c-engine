#include "cengine/texture.h"

#include <GL/glew.h>
#include <GL/gl.h>

#include "cengine/internal.h"

texture_t new_texture(const char *path) {
	texture_t id = {
		.id = 0,
		.width=0,
		.height=0,
		.path=path,
		.internal_format=0,
		.data_format=0,
	};

	// Image convention and OGL texture convention for axis origin are not the same:
	stbi_set_flip_vertically_on_load(1);

	stbi_uc *data = NULL;
	int width, height, channels;

	data = stbi_load(path, &width, &height, &channels, 0);

	engine_assert(data, "Failed to load image.");

	id.width = width;
	id.height = height;

	switch (channels) {
		case 4:
			id.internal_format = GL_RGBA8;
			id.data_format = GL_RGBA;
			break;
		case 3:
			id.internal_format = GL_RGB8;
			id.data_format = GL_RGB;
			break;
		default:
			engine_assert(false, "Unsupported file format");
	}

	engine_assert(id.internal_format != 0 && id.data_format != 0, "Format not supported.");

	glCreateTextures(GL_TEXTURE_2D, 1, &id.id);
	glTextureStorage2D(id.id, 1, id.internal_format, id.width, id.height);

	// TODO: need to study parameters to use some kind of struct document to declare this:
	glTextureParameteri(id.id, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTextureParameteri(id.id, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTextureParameteri(id.id, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTextureParameteri(id.id, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTextureSubImage2D(id.id, 0, 0, 0, id.width, id.height, id.data_format, GL_UNSIGNED_BYTE, data);

	// Since the image data is now in OGL realm, we can deallocate it:
	stbi_image_free(data);

	return id;
}

texture_t new_texture_empty(const uint32_t width, const uint32_t height) {
	texture_t id = {
		.id=0,
		.width=width,
		.height=height,
		.internal_format=GL_RGBA8,
		.data_format=GL_RGBA,
	};

	glCreateTextures(GL_TEXTURE_2D, 1, &id.id);
	glTextureStorage2D(id.id, 1, id.internal_format, id.width, id.height);

	glTextureParameteri(id.id, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTextureParameteri(id.id, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTextureParameteri(id.id, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTextureParameteri(id.id, GL_TEXTURE_WRAP_T, GL_REPEAT);

	return id;
}

void delete_texture(const texture_t id) {
	glDeleteTextures(1, &id.id);
}

void set_data_texture(const texture_t id, const void *data, const uint32_t size) {
	engine_assert(size == id.width * id.height * (id.data_format == GL_RGBA ? 4 : 3), "Declared texture size and data buffer size does not match");
	glTextureSubImage2D(id.id, 0, 0, 0, id.width, id.height, id.data_format, GL_UNSIGNED_BYTE, data);
}

void bind_texture(const texture_t id, const uint32_t slot) {
	glBindTextureUnit(slot, id.id);
}

void unbind_texture(const texture_t id, uint32_t slot) {
	glBindTextureUnit(slot, 0);
	glBindTexture(GL_TEXTURE_2D, 0);
	(void)id;
}
