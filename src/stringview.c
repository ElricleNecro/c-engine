#include "cengine/stringview.h"

StringView cstr_as_stringview(char *cstr) {
	return (StringView) {
		.count = strlen(cstr),
		.data = cstr,
	};
}

StringView stringview_ltrim(StringView sv) {
	size_t i = 0;

	while( i < sv.count && isspace(sv.data[i]) )
		i += 1;

	return (StringView) {
		.count = sv.count - i,
		.data = sv.data + i,
	};
}

StringView stringview_rtrim(StringView sv) {
	size_t i = 0;

	while( i < sv.count && isspace(sv.data[sv.count - 1 - i]) )
		i += 1;

	return (StringView) {
		.count = sv.count - i,
		.data = sv.data,
	};
}

StringView stringview_trim(StringView sv) {
	return stringview_ltrim(stringview_rtrim(sv));
}

StringView stringview_split(StringView *sv, const char delim) {
	size_t i =0;
	while( i < sv->count && sv->data[i] != delim )
		i += 1;

	StringView result = {
		.count = i,
		.data = sv->data,
	};

	if( i < sv->count ) {
		sv->count -= i + 1;
		sv->data += i + 1;
	} else {
		sv->count -= i;
		sv->data += i;
	}

	return result;
}

StringView stringview_split_on_spaces(StringView *sv) {
	size_t i =0;
	while( i < sv->count && ! isspace(sv->data[i]) )
		i += 1;

	StringView result = {
		.count = i,
		.data = sv->data,
	};

	if( i < sv->count ) {
		sv->count -= i + 1;
		sv->data += i + 1;
	} else {
		sv->count -= i;
		sv->data += i;
	}

	return result;
}

StringView stringview_memcopy(StringView sv) {
	StringView copy = {
		.data = malloc(sv.count * sizeof(char)),
		.count = sv.count,
	};

	memcpy(copy.data, sv.data, sv.count);

	return copy;
}

bool stringview_eq(StringView a, StringView b) {
	if( a.count != b.count )
		return false;

	return memcmp(a.data, b.data, a.count) == 0;
}

bool stringview_eq_cstr(StringView a, const char *b) {
	if( a.count != strlen(b) )
		return false;

	return memcmp(a.data, b, a.count) == 0;
}

bool stringview_endwith(StringView sv, const char end) {
	if( sv.count > 0 && sv.data[sv.count - 1] == end)
		return true;
	return false;
}

bool stringview_startwith(StringView sv, const char start) {
	if( sv.count > 0 && sv.data[0] == start)
		return true;
	return false;
}

bool stringview_to_long(StringView sv, long *result) {
	char *endptr = NULL;

	*result = strtol(sv.data, &endptr, 0);

	if( (size_t)(endptr - sv.data) != sv.count ) {
		return false;
	}

	return true;
}

bool stringview_to_ulong(StringView sv, unsigned long *result) {
	char *endptr = NULL;

	*result = strtoul(sv.data, &endptr, 0);

	if( (size_t)(endptr - sv.data) != sv.count ) {
		return false;
	}

	return true;
}

int stringview_to_int(StringView sv) {
	int result = 0;

	for(size_t i = 0; i < sv.count && isdigit(sv.data[i]); i++)
		result = result * 10 + sv.data[i] - '0';

	return result;
}

int load_file(const char *fpath, StringView *content) {
	FILE *file = NULL;
	if( (file = fopen(fpath, "r")) == NULL ) {
		return -1;
	}

	if( fseek(file, 0, SEEK_END) < 0 ) {
		return -1;
	}

	long fsize = ftell(file);
	if( fsize < 0 ) {
		return -1;
	}

	if( fseek(file, 0, SEEK_SET) < 0 ) {
		fprintf(stderr, "ERROR: Could not read file '%s': %s\n", fpath, strerror(errno));
		return errno;
	}

	char *src = NULL;
	if( (src = malloc((size_t)fsize)) == NULL ) {
		fprintf(stderr, "ERROR: Could not allocate memory for file '%s': %s\n", fpath, strerror(errno));
		return errno;
	}

	size_t n = fread(src, sizeof(char), (size_t)fsize, file);
	if( ferror(file) ) {
		return errno;
	}

	fclose(file);

	if( content ){
		content->count = n;
		content->data = src;
	}

	return 0;
}

StringView stringview_search(const StringView sv, const char *motif) {
	StringView find = sv;

	if( strlen(motif) >= sv.count )
		return find;

	size_t motif_len = strlen(motif);
	find.data = sv.data;
	for(size_t idx = 0; idx < (sv.count - motif_len); idx++,find.data++,find.count--) {
		if( strncmp(motif, find.data, motif_len) == 0 ) {
			return find;
		}
	}

	find.data = &sv.data[sv.count-1];
	find.count = 0;

	return find;
}
