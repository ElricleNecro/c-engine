#include "cengine/vertex_array.h"

#include <assert.h>

#include <GL/glew.h>
#include <GL/gl.h>

uint32_t data_type_size(data_type type) {
	switch (type) {
	case FLOAT:
		return sizeof(float);
	case FLOAT2:
		return sizeof(float) * 2;
	case FLOAT3:
		return sizeof(float) * 3;
	case FLOAT4:
		return sizeof(float) * 4;

	case MAT3:
		return sizeof(float) * 3 * 3;
	case MAT4:
		return sizeof(float) * 4 * 4;

	case INT:
		return sizeof(int);
	case INT2:
		return sizeof(int) * 2;
	case INT3:
		return sizeof(int) * 3;
	case INT4:
		return sizeof(int) * 4;

	case BOOL:
		return sizeof(bool);

	case NONE:
	default:
		assert(false);
	}
}

vertex_array new_vertex_array(void) {
	vertex_array id = {0};

	glCreateVertexArrays(1, &id.id);

	return id;
}

void delete_vertex_array(const vertex_array id) {
	glDeleteVertexArrays(1, &id.id);
}

void bind_vertex_array(const vertex_array id) {
	glBindVertexArray(id.id);
}

void unbind_vertex_array(const vertex_array id) {
	glBindVertexArray(id.id);
}

vertex_array set_index_buffer_vertex_array(vertex_array id, const index_buffer vio) {
	id.vio = vio;

	glBindVertexArray(id.id);
	bind_index_buffer(vio);
	glBindVertexArray(0);
	unbind_index_buffer();

	return id;
}

uint32_t get_count_data_type(data_type type) {
	switch( type ) {
		case FLOAT:
			return 1;
		case FLOAT2:
			return 2;
		case FLOAT3:
			return 3;
		case FLOAT4:
			return 4;

		case MAT3:
			return 3;
		case MAT4:
			return 4;

		case INT:
			return 1;
		case INT2:
			return 2;
		case INT3:
			return 3;
		case INT4:
			return 4;

		case BOOL:
			return 1;

		case NONE:
		default:
			assert(false);
	}
}

GLenum data_type_to_GLenum(data_type type) {
	switch( type ) {
		case FLOAT:
		case FLOAT2:
		case FLOAT3:
		case FLOAT4:
		case MAT3:
		case MAT4:
			return GL_FLOAT;
		case INT:
		case INT2:
		case INT3:
		case INT4:
			return GL_INT;
		case BOOL:
			return GL_BOOL;
		case NONE:
		default:
			assert(false);
	}
}

uint32_t calculate_offset_and_stride(const uint64_t n_element, layout_description layout[static n_element]) {
	uint32_t stride = 0;

	uint64_t offset = 0;
	for(uint64_t idx = 0; idx < n_element; idx++) {
		layout[idx].offset = offset;
		offset += layout[idx].size;
		stride += layout[idx].size;
	}

	return stride;
}

vertex_array set_vextex_buffer_vertex_array(vertex_array id, const vertex_buffer vbo, const uint64_t n_element, layout_description layout[static n_element]) {
	id.vbo = vbo;

	glBindVertexArray(id.id);
	bind_vertex_buffer(vbo);

	uint32_t stride = calculate_offset_and_stride(n_element, layout);
	uint32_t index = 0;
	for(uint64_t idx = 0; idx < n_element; idx++) {
		const layout_description element = layout[idx];
		const uint32_t count = get_count_data_type(element.type);

		switch( element.type ) {
			case FLOAT:
			case FLOAT2:
			case FLOAT3:
			case FLOAT4:
				glEnableVertexAttribArray(index);
				glVertexAttribPointer(
					index,
					count,
					data_type_to_GLenum(element.type),
					element.normalised ? GL_TRUE : GL_FALSE,
					stride,
					(const void*)element.offset
				);

				index++;
				break;

			case INT:
			case INT2:
			case INT3:
			case INT4:
			case BOOL:
				glEnableVertexAttribArray(index);
				glVertexAttribIPointer(
					index,
					count,
					data_type_to_GLenum(element.type),
					stride,
					(const void *)element.offset
				);

				index++;
				break;

			case MAT3:
			case MAT4:
				for (uint8_t elem = 0; elem < count; elem++) {
					glEnableVertexAttribArray(index);
					glVertexAttribPointer(
						index,
						count,
						data_type_to_GLenum(element.type),
						element.normalised ? GL_TRUE : GL_FALSE,
						stride,
						(const void *)(element.offset + sizeof(float) * count * elem)
					);
					glVertexAttribDivisor(index, 1);

					index++;
				}
				break;
			case NONE:
				assert(false);
		}
	}

	glBindVertexArray(0);
	unbind_vertex_buffer();

	return id;
}
