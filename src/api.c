#include <GL/glew.h>
#include <GL/gl.h>

#include "cengine/api.h"

void opengl_api_init(void) {
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	glEnable(GL_LINE_SMOOTH);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
}

void opengl_api_set_clear_color(vec4 color) {
	glClearColor(color[0], color[1], color[2], color[3]);
}

void opengl_api_clear(void) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void opengl_api_draw_indexed(vertex_array vao, uint32_t index_count) {
	index_count = index_count == 0 ? vao.vio.count : index_count;
	glDrawElements(GL_TRIANGLES, index_count, GL_UNSIGNED_INT, NULL);
}

void opengl_api_draw_lines(vertex_array vao, uint32_t index_count) {
	glDrawArrays(GL_LINES, 0, index_count);
	(void)vao;
}

void opengl_api_set_line_width(float width) {
	glLineWidth(width);
}

void opengl_api_viewport(int x, int y, int width, int height) {
	glViewport(x, y, width, height);
}

void opengl_api_setup(void) {
	printf("Doing Setup.\n");

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
	SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
}

uint32_t opengl_api_get_sdl_flags(void) {
	return SDL_WINDOW_OPENGL;
}

result(SDL_Renderer) opengl_api_create_context(SDL_Window *window, int *width, int *height) {
	printf("Creating context.\n");

	SDL_Renderer *context = SDL_GL_CreateContext(window);
	if( context == NULL ) {
		return is_error(SDL_Renderer, SDL_GetError());
	}

	SDL_GL_MakeCurrent(window, context);
	SDL_GL_SetSwapInterval(1); // Enable vsync
	SDL_GL_GetDrawableSize(window, width, height);

	printf("Initialising GLEW.\n");
	GLenum res = glewInit();
	if( res != GLEW_OK ) {
		return is_error(SDL_Renderer, (const char*)glewGetErrorString(res));
	}

	return is_valid(SDL_Renderer, context);
}

void opengl_api_destroy_context(SDL_Renderer *context) {
	SDL_GL_DeleteContext(context);
	SDL_DestroyRenderer(context);
}

void opengl_api_swap_window(SDL_Window *window) {
	SDL_GL_SwapWindow(window);
}

engine_api_t get_api(graphic_api api) {
	switch(api) {
		case API_OPENGL:
			return (engine_api_t){
				.init = opengl_api_init,
				.viewport = opengl_api_viewport,

				.set_clear_color = opengl_api_set_clear_color,
				.clear = opengl_api_clear,

				.draw_indexed = opengl_api_draw_indexed,

				.draw_lines = opengl_api_draw_lines,
				.set_line_width = opengl_api_set_line_width,

				._setup = opengl_api_setup,
				._get_sdl_flags = opengl_api_get_sdl_flags,
				._create_context = opengl_api_create_context,
				._destroy_context = opengl_api_destroy_context,
				._swap_window = opengl_api_swap_window,
			};

		case API_NONE:
		default:
			break;
	}

	return (engine_api_t){
		.init = NULL,
		.set_clear_color = NULL,
		.clear = NULL,
		.draw_indexed = NULL,
		.draw_lines = NULL,
		.set_line_width = NULL,
		.viewport = NULL,
		._setup = NULL,
		._get_sdl_flags = NULL,
		._create_context = NULL,
		._destroy_context = NULL,
		._swap_window = NULL,
	};
}
