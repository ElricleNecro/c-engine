#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <dlfcn.h>

#include "cengine/engine.h"

void* get_plugin_handle(const int argc, char * const * argv) {
	void *handle = NULL;

	for(int arg = 0; arg < argc; arg++) {
		if( strncmp("--plugin", argv[arg], 9) == 0 ) {
			arg++;
			fprintf(stderr, "Opening '%s'\n", argv[arg]);
			handle = dlopen(argv[arg], RTLD_NOW | RTLD_LOCAL);
		}
	}

	return handle;
}

struct plugin_t {
	void *handle;

	engine_configuration_t (*config)(void);
};

struct plugin_t plugin_open(const int argc, char * const * argv) {
	struct plugin_t plugin = {
		.handle=get_plugin_handle(argc, argv),
		.config=NULL,
	};

	if( plugin.handle == NULL ) {
		
		fprintf(stderr, "No handle loaded: '%s'.", dlerror());
		exit(EXIT_FAILURE);
	}

	plugin.config = dlsym(plugin.handle, "configure");
	if( plugin.config == NULL ) {
		fprintf(stderr, "Unable to load `configure` function.");
		dlclose(plugin.handle);
		exit(EXIT_FAILURE);
	}

	return plugin;
}

void plugin_close(struct plugin_t *plugin) {
	dlclose(plugin->handle), plugin->handle = NULL;

	plugin->config = NULL;
}

int main(int argc, char **argv) {
	// load plugin
	struct plugin_t plugin = plugin_open(argc, argv);

	// call plugin engine init
	engine_configuration_t config = plugin.config();

	// init engine
	int return_value = EXIT_SUCCESS;
	result(engine_t) res = engine_new(config);
	if( ! res.valid ) {
		puts(res.error);
		return_value = EXIT_FAILURE;
		goto done;
	}
	engine_t game = res.data;

	// run engine main loop
	engine_main_loop(game);

	// free engine
	engine_free(game);

done:
	// call plugin free
	plugin_close(&plugin);

	return return_value;
}
