#include "cengine/framebuffer.h"

#include <assert.h>
#include <stdio.h>

#include <GL/glew.h>
#include <GL/gl.h>

#include "cengine/internal.h"

static inline GLenum texture_target(bool multi_sampled) {
	return multi_sampled ? GL_TEXTURE_2D_MULTISAMPLE : GL_TEXTURE_2D;
}

static inline void create_texture(const bool multi_sample, uint32_t *ids, const size_t count) {
	glCreateTextures(texture_target(multi_sample), count, ids);
}

static inline void bind_texture(bool multi_sample, uint32_t id) {
	glBindTexture(texture_target(multi_sample), id);
}

static inline void attach_color_texture(const uint32_t id, const bool multi_sample, const int samples, const GLenum internal_format, const GLenum format, const uint32_t width, const uint32_t height, const size_t index) {
	if (multi_sample) {
		glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, samples, format, width, height, GL_FALSE);
	} else {
		glTexImage2D(GL_TEXTURE_2D, 0, internal_format, width, height, 0, format, GL_UNSIGNED_BYTE, NULL);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	}

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + index, texture_target(multi_sample), id, 0);
}

static inline void attach_depth_texture(const uint32_t id, const bool multi_sample, const int samples, const GLenum format, const GLenum attachment_type, const uint32_t width, const uint32_t height) {
	if (multi_sample) {
		glTexStorage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, samples, format, width, height, GL_FALSE);
	} else {
		glTexStorage2D(GL_TEXTURE_2D, 1, format, width, height);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	}

	//glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, this->spec.width, this->spec.height, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, nullptr);
	glFramebufferTexture2D(GL_FRAMEBUFFER, attachment_type, texture_target(multi_sample), id, 0);
}

static inline GLenum solace_texture_format_to_gl(const framebuffer_texture_format format) {
	switch (format) {
		// Color:
		case RGBA8:
			return GL_RGBA8;
		case RED_INTEGER:
			return GL_RED_INTEGER;
			// Depth/stencil:
		case DEPTH24STENCIL8:
		case None:
			engine_assert(false, "We should not reach this statement!");
			return (GLenum)0;
	}

	engine_assert(false, "We should not reach this statement!");
	return (GLenum)0;
}

framebuffer new_framebuffer(const framebuffer_spec spec) {
	framebuffer buf = {0};

	uint8_t corr = 0;
	for(size_t idx=0; idx <= MAX_COLOR_ATTACHMENTS; idx++) {
		switch(spec.attachment[idx].format) {
			case DEPTH24STENCIL8:
				buf.depth_attachment_specs = spec.attachment[idx];
				corr++;
				break;
				
			case RGBA8:
			case RED_INTEGER:
				buf.color_attachment_specs[buf.color_attachment_registered] = spec.attachment[idx];
				buf.color_attachment_registered++;
				break;

			case None:
			default:
				continue;
		}
	}

	buf = invalidate(buf);

	return buf;
}

framebuffer delete_framebuffer(framebuffer id) {
	if (id.id != 0) {
		glDeleteFramebuffers(1, &id.id);
		glDeleteTextures(id.color_attachment_registered, id.color_attachments);
		glDeleteTextures(1, &id.depth_attachment);
	}

	return id;
}

int read_pixel(const framebuffer id, const uint32_t attachment_idx, const uint32_t x, const uint32_t y) {
	engine_assert(attachment_idx < id.color_attachment_registered, "Wrong attachment id.");

	glReadBuffer(GL_COLOR_ATTACHMENT0 + attachment_idx);

	int pixel_data = 0;
	glReadPixels(x, y, 1, 1, GL_RED_INTEGER, GL_INT, &pixel_data);

	return pixel_data;
}

void clear_color_attachment(const framebuffer id, const size_t index, const int value) {
	engine_assert(index < id.color_attachment_registered, "Wrong attachment id.");

	glClearTexImage(
		id.color_attachments[index],
		0,
		solace_texture_format_to_gl(id.color_attachment_specs[index].format),
		GL_INT,
		&value
	);
}

framebuffer invalidate(framebuffer id) {
	if( id.id != 0 ) {
		glDeleteFramebuffers(1, &id.id);
		glDeleteTextures(id.color_attachment_registered, id.color_attachments);
		glDeleteTextures(1, &id.depth_attachment);

		for(size_t idx=0; idx < id.color_attachment_registered; idx++)
			id.color_attachments[idx] = 0;
		id.depth_attachment = 0;
		id.id = 0;
	}

	glCreateFramebuffers(1, &id.id);
	glBindFramebuffer(GL_FRAMEBUFFER, id.id);

	bool multi_sample = id.spec.samples > 1;
	if( id.color_attachment_registered > 0 ) {
		create_texture(multi_sample, id.color_attachments, id.color_attachment_registered);

		for(size_t idx=0; idx < id.color_attachment_registered; idx++) {
			bind_texture(multi_sample, id.color_attachments[idx]);

			switch(id.color_attachment_specs[idx].format) {
				case RGBA8:
					attach_color_texture(
						id.color_attachments[idx],
						multi_sample,
						id.spec.samples,
						GL_RGBA8,
						GL_RGBA,
						id.spec.width,
						id.spec.height,
						idx
					);
					break;

				case RED_INTEGER:
					attach_color_texture(
						id.color_attachments[idx],
						multi_sample,
						id.spec.samples,
						GL_R32I,
						GL_RED_INTEGER,
						id.spec.width,
						id.spec.height,
						idx
					);
					break;

				case None:
				case DEPTH24STENCIL8:
					assert(false);
					break;
			}
		}
	}

	if( id.depth_attachment_specs.format != None ) {
		create_texture(multi_sample, &id.depth_attachment, 1);
		bind_texture(multi_sample, id.depth_attachment);

		switch (id.depth_attachment_specs.format) {
		case DEPTH24STENCIL8:
			attach_depth_texture(
				id.depth_attachment,
				multi_sample,
				id.spec.samples,
				GL_DEPTH24_STENCIL8,
				GL_DEPTH_STENCIL_ATTACHMENT,
				id.spec.width,
				id.spec.height
			);
			break;

		case RED_INTEGER:
		case RGBA8:
		case None:
			engine_assert(false, "Should be unreachable.");
			break;
		}
	}

	if( id.color_attachment_registered > 1 ) {
		engine_assert(id.color_attachment_registered <= 4, "Can't have more than 4 colors attachement to a framebuffer.");
		GLenum buffers[4] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3 };
		glDrawBuffers(id.color_attachment_registered, buffers);
	} else if( id.color_attachment_registered == 0 ) {
		glDrawBuffer(GL_NONE);
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	return id;
}

framebuffer resize(framebuffer id, const uint32_t width, const uint32_t height) {
	if (width == 0 || height == 0 || width > MAX_FRAMEBUFFER_SIZE || height > MAX_FRAMEBUFFER_SIZE) {
		fprintf(stderr, "Trying to resize framebuffer with invalid size (%u, %u)!", width, height);
		return id;
	}

	id.spec.width = width;
	id.spec.height = height;

	return invalidate(id);
}

void bind(const framebuffer id) {
	glBindFramebuffer(GL_FRAMEBUFFER, id.id);
	glViewport(0, 0, id.spec.width, id.spec.height);
}

void unbind(const framebuffer id) {
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	(void)id;
}
