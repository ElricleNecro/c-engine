#include "cengine/engine.h"

#include <GL/gl.h>

result(engine_t) engine_new(engine_configuration_t config) {
	result(engine_t) res;

	if( config.on_update == NULL ) {
		res = is_error(engine_t, "No on_update function available.");
		goto out_function;
	}

	engine_t game = {
		.tpf = 1000.f / (config.fps != 0.f ? config.fps : 60.f),
		.api = config.api,
		.graphic_api = get_api(config.api),
		.on_init = config.on_init,
		.on_resize = config.on_resize,
		.on_update = config.on_update,
		.on_event = config.on_event,
		.on_clean = config.on_clean,
		.data = config.data,
		.window = NULL,
		.context = NULL,
		.events = new_events_t(config.width, config.height),
	};

	if( SDL_Init(SDL_INIT_VIDEO) < 0 ) {
		res = is_error(engine_t, SDL_GetError());
		goto out_function;
	}

	game.graphic_api._setup();

	game.window = SDL_CreateWindow(
		"Raycaster",
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		game.events.width,
		game.events.height,
		SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI | game.graphic_api._get_sdl_flags()
	);

	if( game.window == NULL ) {
		res = is_error(engine_t, SDL_GetError());
		goto window_error;
	}

	result(SDL_Renderer) context = game.graphic_api._create_context(game.window, &game.events.width, &game.events.height);
	if( ! context.valid ) {
		res = is_error(engine_t, context.error);
		goto context_error;
	}

	game.graphic_api.viewport(0, 0, game.events.width, game.events.height);

	return is_valid(engine_t, game);

context_error:
	SDL_DestroyWindow(game.window);
window_error:
	SDL_Quit();
out_function:
	return res;
}

void engine_free(engine_t game) {
	if( game.context != NULL )
		game.graphic_api._destroy_context(game.context);

	if( game.window != NULL )
		SDL_DestroyWindow(game.window);

	SDL_Quit();
}

static inline void engine_resize(engine_t game) {
	game.graphic_api.viewport(0, 0, game.events.width, game.events.height);
}

void engine_main_loop(engine_t game) {
	bool running = true;
	uint32_t previous = SDL_GetTicks64(), current = 0;

	if( game.api == API_NONE )
		return ;

	game.graphic_api.init();

	if( game.on_init != NULL )
		game.on_init(game, game.data);

	while( running ) {
		current = SDL_GetTicks64();
		uint32_t elapsed = current - previous;
		if( elapsed < game.tpf ) {
			SDL_Delay(game.tpf - elapsed);
		}

		while( update_events_t(&game.events) ) {
			if( game.events.quit || (game.events.close_window && SDL_GetWindowID(game.window) == game.events.winID) ) {
				running = false;
				goto cleanup;
			}

			if( game.events.minimized )
				continue;

			if( game.events.resized ) {
				engine_resize(game);
				if( game.on_resize != NULL )
					game.on_resize(game.events.width, game.events.height, game.data);
			}

			if( game.on_event != NULL )
				game.events.event_used = game.on_event(&game.events, game.data);
		}

		game.on_update(game, elapsed / 1000.f, game.data);

		game.graphic_api._swap_window(game.window);

		previous = current;
	}

cleanup:
	if( game.on_clean != NULL )
		game.on_clean(game, game.data);
}
