#include "cengine/shader.h"

#include <assert.h>

#include <GL/glew.h>
#include <GL/gl.h>

#include "cengine/stringview.h"

static inline shader_type stringview_to_shader_type(const StringView type) {
	if( strncmp(type.data, "vertex", type.count) == 0 )
		return SHADER_VERTEX;
	else if( strncmp(type.data, "fragment", type.count) == 0 )
		return SHADER_FRAGMENT;
	else if( strncmp(type.data, "tesselation_control", type.count) == 0 )
		return SHADER_TESSELATION_CONTROL;
	else if( strncmp(type.data, "tesselation_evaluation", type.count) == 0 )
		return SHADER_TESSELATION_EVALUATION;
	else if( strncmp(type.data, "geometry", type.count) == 0 )
		return SHADER_GEOMETRY;
	else if( strncmp(type.data, "fragment", type.count) == 0 )
		return SHADER_FRAGMENT;
	else if( strncmp(type.data, "compute", type.count) == 0 )
		return SHADER_COMPUTE;
	else
		return SHADER_NUMBERS;
}

static inline GLenum shader_type_to_opengl(shader_type type) {
	switch(type) {
		case SHADER_VERTEX:
			return GL_VERTEX_SHADER;
		case SHADER_TESSELATION_CONTROL:
			return GL_TESS_CONTROL_SHADER;
		case SHADER_TESSELATION_EVALUATION:
			return GL_TESS_EVALUATION_SHADER;
		case SHADER_GEOMETRY:
			return GL_GEOMETRY_SHADER;
		case SHADER_FRAGMENT:
			return GL_FRAGMENT_SHADER;
		case SHADER_COMPUTE:
			return GL_COMPUTE_SHADER;
		case SHADER_NUMBERS:
		default:
			return 0;
	}
}

static GLenum compile(const shader_type type, const StringView src) {
	uint32_t shader_id = glCreateShader(shader_type_to_opengl(type));

	{
		int len = src.count;
		glShaderSource(shader_id, 1, (const char * const *)&src.data, &len);
	}
	glCompileShader(shader_id);

	GLint result = GL_FALSE;
	glGetShaderiv(shader_id, GL_COMPILE_STATUS, &result);
	if( result == GL_FALSE ) {
		int info_length = 0;
		glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &info_length);

		char error_msg[info_length + 1];
		glGetShaderInfoLog(shader_id, info_length, NULL, &error_msg[0]);

		glDeleteShader(shader_id);
		shader_id = 0;

		fprintf(stderr, "%s\n", error_msg);
		switch (type) {
			case SHADER_VERTEX:
				fprintf(stderr, "Error while compiling vertex shader.");
				assert(false);
				break;
			case SHADER_TESSELATION_CONTROL:
				fprintf(stderr, "Error while compiling tesselation control shader.");
				assert(false);
				break;
			case SHADER_TESSELATION_EVALUATION:
				fprintf(stderr, "Error while compiling tesselation evaluation shader.");
				assert(false);
				break;
			case SHADER_GEOMETRY:
				fprintf(stderr, "Error while compiling geometry shader.");
				assert(false);
				break;
			case SHADER_FRAGMENT:
				fprintf(stderr, "Error while compiling fragment shader.");
				assert(false);
				break;
			case SHADER_COMPUTE:
				fprintf(stderr, "Error while compiling compute shader.");
				assert(false);
				break;
			case SHADER_NUMBERS:
			default:
				fprintf(stderr, "Unexpected shader type.");
				assert(false);
		}
	}

	return shader_id;
}

static shader shader_parse_source(StringView src) {
	shader program = {.id = glCreateProgram(), {0}};

	static const char * const motif = "#type";
	StringView start = stringview_search(src, motif);

	do {
		StringView type = stringview_split(&start, '\n');
		StringView next = stringview_search(start, motif);
		start = stringview_minus(start, next);

		stringview_split_on_spaces(&type);
		type = stringview_trim(type);

		shader_type stype = stringview_to_shader_type(type);
		program.shaders[stype] = compile(stype, start);
		assert(program.shaders[stype] != 0);

		start = next;
	} while(start.count != 0);

	for(size_t idx=0; idx < SHADER_NUMBERS; idx++) {
		if( program.shaders[idx] != 0 )
			glAttachShader(program.id, program.shaders[idx]);
	}

	glLinkProgram(program.id);
	GLint success = 1;
	glGetProgramiv(program.id, GL_LINK_STATUS, &success);

	if( success == GL_FALSE ) {
		int info_length = 0;
		glGetProgramiv(program.id, GL_INFO_LOG_LENGTH, &info_length);

		char error_msg[info_length + 1];
		glGetProgramInfoLog(program.id, info_length, NULL, &error_msg[0]);

		glDeleteProgram(program.id);

		for(size_t idx=0; idx < SHADER_NUMBERS; idx++)
			if( program.shaders[idx] != 0 )
				glDeleteShader(program.shaders[idx]);

		fprintf(stderr, "Error while creating the shader program: %s.\n", error_msg);
		assert(false);
	}

	for(size_t idx=0; idx < SHADER_NUMBERS; idx++) {
		if( program.shaders[idx] != 0 )
			glDetachShader(program.id, program.shaders[idx]);
	}

	return program;
}

shader new_shader_from_string(char *src) {
	StringView view = {.data=src, .count=strlen(src)};

	return shader_parse_source(view);
}

void clean_stringview(StringView *src) {
	if( src->data != NULL )
		free(src->data), src->data = NULL;
}

shader new_shader_from_file(const char *fname) {
	StringView src __attribute__((__cleanup__(clean_stringview))) = {0};
	if( load_file(fname, &src) != 0 )
		exit(EXIT_FAILURE);

	return shader_parse_source(src);
}

void delete_shader(const shader id) {
	for(size_t idx=0; idx < SHADER_NUMBERS; idx++)
		if( id.shaders[idx] != 0 )
			glDeleteShader(id.shaders[idx]);

	glDeleteProgram(id.id);
}

void bind_shader(shader id) {
	glUseProgram(id.id);
}

void unbind_shader(shader id) {
	glUseProgram(0);
	(void)id;
}

void set_mat3_shader(shader id, const char *name, const mat3 v) {
	GLint uniform = glGetUniformLocation(id.id, name);
	glUniformMatrix3fv(uniform, 1, GL_FALSE, &v[0][0]);
}

void set_mat4_shader(shader id, const char *name, const mat4 v) {
	GLint uniform = glGetUniformLocation(id.id, name);
	glUniformMatrix4fv(uniform, 1, GL_FALSE, &v[0][0]);
}

void set_int_shader(shader id, const char *name, const int v) {
	GLint uniform = glGetUniformLocation(id.id, name);
	glUniform1i(uniform, v);
}

void set_int_array_shader(shader id, const char *name, const size_t count, const int *v) {
	GLint uniform = glGetUniformLocation(id.id, name);
	glUniform1iv(uniform, count, v);
}

void set_float_shader(shader id, const char *name, const float v) {
	GLint uniform = glGetUniformLocation(id.id, name);
	glUniform1f(uniform, v);
}

void set_vec2_shader(shader id, const char *name, const vec2 v) {
	GLint uniform = glGetUniformLocation(id.id, name);
	glUniform2f(uniform, v[0], v[1]);
}

void set_vec3_shader(shader id, const char *name, const vec3 v) {
	GLint uniform = glGetUniformLocation(id.id, name);
	glUniform3f(uniform, v[0], v[1], v[2]);
}

void set_vec4_shader(shader id, const char *name, const vec4 v) {
	GLint uniform = glGetUniformLocation(id.id, name);
	glUniform4f(uniform, v[0], v[1], v[2], v[3]);
}
