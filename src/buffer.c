#include "cengine/buffer.h"

#include <GL/glew.h>
#include <GL/gl.h>

vertex_buffer new_vertex_buffer_dynamic(const uint32_t size) {
	vertex_buffer id = 0;

	glCreateBuffers(1, &id);

	glBindBuffer(GL_ARRAY_BUFFER, id);
	glBufferData(GL_ARRAY_BUFFER, size, NULL, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	return id;
}

vertex_buffer new_vertex_buffer_static(const void *data, const uint32_t size) {
	vertex_buffer id = 0;

	glCreateBuffers(1, &id);

	glBindBuffer(GL_ARRAY_BUFFER, id);
	glBufferData(GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	return id;
}

void bind_vertex_buffer(const vertex_buffer id) {
	glBindBuffer(GL_ARRAY_BUFFER, id);
}

void unbind_vertex_buffer(void) {
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void set_data_vertex_buffer(const vertex_buffer id, const void *data, const uint32_t size) {
	bind_vertex_buffer(id);
	glBufferSubData(GL_ARRAY_BUFFER, 0, size, data);
	unbind_vertex_buffer();
}

void delete_vertex_buffer(const vertex_buffer id) {
	glDeleteBuffers(1, &id);
}

index_buffer new_index_buffer_static(const uint32_t *indices, const uint32_t count) {
	index_buffer id = {.id = 0, .count = count};

	glCreateBuffers(1, &id.id);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, id.id);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, id.count * sizeof(uint32_t), indices, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	return id;
}

void bind_index_buffer(const index_buffer id) {
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, id.id);
}

void unbind_index_buffer(void) {
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void delete_index_buffer(const index_buffer id) {
	glDeleteBuffers(1, &id.id);
}
