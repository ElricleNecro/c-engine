#pragma once

#include <stdbool.h>

typedef struct map_data_t {
	int x, y;
	int s;
} map_data_t;

typedef struct point_t {
	float x, y;
	union {
		float dx;
		float offset_x;
	};
	union {
		float dy;
		float offset_y;
	};
	float angle;
} point_t;

typedef struct button_t {
	bool z, s, q, d;
} button_t;
