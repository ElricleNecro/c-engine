#pragma once

#include <stdint.h>

#include "cengine/config.h"

typedef uint32_t vertex_buffer;
typedef struct index_buffer {
	uint32_t id;
	uint64_t count;
} index_buffer;

CENGINE_API vertex_buffer new_vertex_buffer_dynamic(const uint32_t size);
CENGINE_API vertex_buffer new_vertex_buffer_static(const void *data, const uint32_t size);
CENGINE_API void bind_vertex_buffer(const vertex_buffer id);
CENGINE_API void unbind_vertex_buffer(void);
CENGINE_API void set_data_vertex_buffer(const vertex_buffer id, const void *data, const uint32_t size);
CENGINE_API void delete_vertex_buffer(const vertex_buffer id);

CENGINE_API index_buffer new_index_buffer_static(const uint32_t *indices, const uint32_t size);
CENGINE_API void bind_index_buffer(const index_buffer id);
CENGINE_API void unbind_index_buffer(void);
CENGINE_API void delete_index_buffer(const index_buffer id);
