#pragma once

#include <stdbool.h>
#include <stdint.h>

#include "cengine/buffer.h"
#include "cengine/config.h"

typedef struct vertex_array {
	uint32_t id;

	vertex_buffer vbo;
	index_buffer vio;
} vertex_array;

typedef enum data_type {
	NONE = 0,

	FLOAT,
	FLOAT2,
	FLOAT3,
	FLOAT4,

	MAT3,
	MAT4,

	INT,
	INT2,
	INT3,
	INT4,

	BOOL
} data_type;

typedef struct layout_description {
	const char *name;
	data_type type;

	uint32_t size;
	uint64_t offset;

	bool normalised;
} layout_description;

CENGINE_API vertex_array new_vertex_array(void);
CENGINE_API void delete_vertex_array(const vertex_array id);
CENGINE_API void bind_vertex_array(const vertex_array id);
CENGINE_API void unbind_vertex_array(const vertex_array id);
CENGINE_API vertex_array set_index_buffer_vertex_array(vertex_array id, const index_buffer vio);

CENGINE_API uint32_t get_count_data_type(data_type type);
CENGINE_API uint32_t data_type_size(data_type type);

CENGINE_API uint32_t calculate_offset_and_stride(const uint64_t n_element, layout_description layout[static n_element]);
CENGINE_API vertex_array set_vextex_buffer_vertex_array(vertex_array id, const vertex_buffer vbo, const uint64_t n_element, layout_description layout[n_element]);
