#pragma once

#define result(type) struct _result_##type
#define define_result(type) result(type) {bool valid; type data; const char *error;}
#define define_result_ptr(type) result(type) {bool valid; type *data; const char *error;}
#define is_valid(type, blob) (result(type)){.valid = true, .error = NULL, .data = (blob)}
#define is_error(type, msg) (result(type)){.valid = false, .error = msg}
