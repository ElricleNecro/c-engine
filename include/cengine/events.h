#pragma once

#include <stdbool.h>

#include <SDL2/SDL.h>

typedef struct events_t {
	SDL_Event evt;
	SDL_Scancode last_register;

	uint32_t winID;

	bool kPress[SDL_NUM_SCANCODES];
	bool kMouse[8];

	unsigned int x, y;
	int dx, dy;
	int wx, wy;

	bool close_window, resized, minimized, event_used, quit;

	int width;
	int height;
} events_t;

static inline events_t new_events_t(unsigned int width, unsigned int height) {
	return (events_t){
		.evt = {0},
		.winID = 0,
		.last_register = SDL_NUM_SCANCODES,
		.kPress = { false },
		.kMouse = { false },
		.x = 0,
		.y = 0,
		.dx = 0,
		.dy = 0,
		.wx = 0,
		.wy = 0,
		.close_window = false,
		.resized = false,
		.minimized = false,
		.event_used = false,
		.quit = false,
		.width = width,
		.height = height,
	};
}

bool update_events_t(events_t *evt);

// vim:set ft=c:
