#pragma once

#include <stdint.h>
#include <stdlib.h>

#include <cglm/cglm.h>

#include "cengine/config.h"

typedef enum shader_type {
	SHADER_VERTEX = 0,
	SHADER_TESSELATION_CONTROL,
	SHADER_TESSELATION_EVALUATION,
	SHADER_GEOMETRY,
	SHADER_FRAGMENT,
	SHADER_COMPUTE,
	SHADER_NUMBERS
} shader_type;

typedef struct shader {
	uint32_t id;
	uint32_t shaders[SHADER_NUMBERS];
} shader;

CENGINE_API shader new_shader_from_string(char *src);
CENGINE_API shader new_shader_from_file(const char *fname);
CENGINE_API void delete_shader(const shader id);

CENGINE_API void bind_shader(shader id);
CENGINE_API void unbind_shader(shader id);

CENGINE_API void set_mat3_shader(shader id, const char *name, const mat3 v);
CENGINE_API void set_mat4_shader(shader id, const char *name, const mat4 v);

CENGINE_API void set_int_shader(shader id, const char *name, const int v);
CENGINE_API void set_int_array_shader(shader id, const char *name, const size_t count, const int *v);

CENGINE_API void set_float_shader(shader id, const char *name, const float v);
CENGINE_API void set_vec2_shader(shader id, const char *name, const vec2 v);
CENGINE_API void set_vec3_shader(shader id, const char *name, const vec3 v);
CENGINE_API void set_vec4_shader(shader id, const char *name, const vec4 v);
