#pragma once

#ifdef CENGINE_IMPL
#define CENGINE_API
#elif __cpluplus
#dfefine CENGINE_API extern "C"
#else
#define CENGINE_API extern
#endif
