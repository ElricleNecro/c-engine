#pragma once

#include <math.h>

#include "cengine/types.h"

static inline float limit_angle(float angle) {
	if( angle < 0 )
		angle += 2.f * M_PI;
	else if( angle >= 2.f * M_PI )
		angle -= 2.f * M_PI;

	return angle;
}

static inline float radians(const float angle) {
	return angle * M_PI / 180.f;
}

static inline float min(const float a, const float b) {
	return a > b ? b : a;
}

static inline float signf(float x) {
	return x < 0.f ? -1.f : 1.f;
}

static inline float signi(int x) {
	return x < 0.f ? -1.f : 1.f;
}

#define sign(x) _Generic(x, float: signf(x), default: signi(x))

static inline float square_dist(const point_t a, const point_t b) {
	return (b.x - a.x) * (b.x - a.x) + (b.y - a.y) * (b.y - a.y);
}
