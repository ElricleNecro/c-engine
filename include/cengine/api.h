#pragma once

#include <SDL2/SDL.h>
#include <cglm/cglm.h>

#include "cengine/result.h"
#include "cengine/vertex_array.h"

typedef enum graphic_api {
	API_NONE = 0,
	API_OPENGL,
} graphic_api;

typedef void (*f_engine_api_init)(void);
typedef void (*f_engine_api_set_clear_color)(vec4);
typedef void (*f_engine_api_clear)(void);
typedef void (*f_engine_api_draw_indexed)(vertex_array,uint32_t);
typedef void (*f_engine_api_draw_lines)(vertex_array,uint32_t);
typedef void (*f_engine_api_set_line_width)(float);
typedef void (*f_engine_api_viewport)(int,int,int,int);

define_result_ptr(SDL_Renderer);
typedef result(SDL_Renderer) (*f_enging_api_create_context)(SDL_Window*,int*,int*);
typedef void (*f_enging_api_destroy_context)(SDL_Renderer*);
typedef uint32_t (*f_engine_api_get_sdl_flags)(void);
typedef void (*f_engine_api_setup_api)(void);
typedef void (*f_enging_api_swap_window)(SDL_Window*);

typedef struct _engine_api_t {
	f_engine_api_init init;

	f_engine_api_set_clear_color set_clear_color;
	f_engine_api_clear clear;

	f_engine_api_draw_indexed draw_indexed;
	f_engine_api_draw_lines draw_lines;

	f_engine_api_set_line_width set_line_width;

	f_engine_api_viewport viewport;

	f_engine_api_setup_api _setup;
	f_engine_api_get_sdl_flags _get_sdl_flags;
	f_enging_api_create_context _create_context;
	f_enging_api_destroy_context _destroy_context;
	f_enging_api_swap_window _swap_window;
} engine_api_t;

engine_api_t get_api(graphic_api api);
