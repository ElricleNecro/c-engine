#pragma once

#include <stdint.h>
#include <stdlib.h>

#include "cengine/config.h"

typedef struct uniform_buffer {
	uint32_t id;
} uniform_buffer;

CENGINE_API uniform_buffer new_uniform_buffer(const size_t size, const uint32_t binding);
CENGINE_API void delete_uniform_buffer(const uniform_buffer id);

CENGINE_API void set_data_uniform_buffer(const uniform_buffer id, const void *data, const size_t size, const size_t offset);
