#pragma once

#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>

#include "cengine/config.h"

#define MAX_COLOR_ATTACHMENTS 16

#ifndef MAX_FRAMEBUFFER_SIZE
#define MAX_FRAMEBUFFER_SIZE 8192
#endif

typedef enum framebuffer_texture_format {
	None = 0,

	// Color:
	RGBA8,
	RED_INTEGER,

	// Depth/stencil:
	DEPTH24STENCIL8,

	// Defaults:
	Depth = DEPTH24STENCIL8,
} framebuffer_texture_format;

typedef struct framebuffer_attachment_specification {
	enum framebuffer_texture_format format;
} framebuffer_attachment_specification;

typedef struct framebuffer_spec {
	uint32_t width, height;
	uint32_t samples;
	bool swap_chain_target;

	framebuffer_attachment_specification attachment[MAX_COLOR_ATTACHMENTS+1];
} framebuffer_spec;

typedef struct framebuffer {
	uint32_t id;
	uint32_t depth_attachment;
	uint32_t color_attachment_registered;
	uint32_t color_attachments[MAX_COLOR_ATTACHMENTS];

	framebuffer_spec spec;
	framebuffer_attachment_specification depth_attachment_specs;
	framebuffer_attachment_specification color_attachment_specs[MAX_COLOR_ATTACHMENTS];
} framebuffer;

CENGINE_API framebuffer new_framebuffer(const framebuffer_spec spec);
CENGINE_API framebuffer delete_framebuffer(framebuffer id);

CENGINE_API int read_pixel(const framebuffer id, const uint32_t attachment_idx, const uint32_t x, const uint32_t y);
CENGINE_API void clear_color_attachment(const framebuffer id, const size_t index, const int value);
CENGINE_API framebuffer invalidate(framebuffer id);
CENGINE_API framebuffer resize(framebuffer id, const uint32_t width, const uint32_t height);
CENGINE_API void bind(const framebuffer id);
CENGINE_API void unbind(const framebuffer id);
