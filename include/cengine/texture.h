#pragma once

#include <stdint.h>
#include <stdbool.h>

#include "cengine/image.h"
#include "cengine/config.h"

typedef struct texture_t {
	uint32_t id, width, height;
	const char *path;

	int internal_format, data_format;
} texture_t;

CENGINE_API texture_t new_texture(const char *path);
CENGINE_API texture_t new_texture_empty(const uint32_t width, const uint32_t height);
CENGINE_API void delete_texture(const texture_t id);

CENGINE_API void set_data_texture(const texture_t id, const void *data, const uint32_t size);

CENGINE_API void bind_texture(const texture_t id, const uint32_t slot);
CENGINE_API void unbind_texture(const texture_t id, uint32_t slot);
