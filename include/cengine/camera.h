#pragma once

#include <cglm/cglm.h>

#include "cengine/config.h"

typedef enum camera_kind {
	PERSPECTIVE,
	ORTHOGRAPHIC,
} camera_kind;

typedef struct camera_t {
	float aspect_ratio;
	mat4 view, projection, view_projection;
	vec3 position;

	camera_kind kind;

	union {
		struct {
			float fov, near, far;
			float pitch, yaw, roll;
		};
		struct {
			bool rotatable;
			float zoom_level;
		};
	};
} camera_t;

CENGINE_API camera_t new_camera_perspective(float fov, float aspect_ratio, vec3 eye, vec3 center, vec3 up);
CENGINE_API camera_t new_camera_orthographic(float aspect_ratio, bool rotatable);

CENGINE_API camera_t update_view_camera(camera_t cam);
CENGINE_API camera_t update_aspect_ratio(camera_t cam, const float aspect_ratio);

static inline void setup_ortho_camera(const float aspect_ratio, const float zoom_level, mat4 proj) {
	glm_ortho(-aspect_ratio * zoom_level, aspect_ratio * zoom_level, -zoom_level, zoom_level, -1.f, 1.f, proj);
}
