#pragma once

#include <assert.h>

#define engine_assert(test, msg, ...) if( !(test) ) {fprintf(stderr, msg, ##__VA_ARGS__); assert(test);}
