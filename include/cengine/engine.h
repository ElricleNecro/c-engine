#pragma once

#include <SDL2/SDL.h>

#include <GL/glew.h>
#include <GL/gl.h>

#include "cengine/api.h"
#include "cengine/events.h"
#include "cengine/result.h"

struct engine_t;
typedef void (*on_init_function)(const struct engine_t, void*);
typedef bool (*on_resize_function)(const int, const int, void*);
typedef void (*on_update_function)(const struct engine_t, const float, void*);
typedef bool (*on_event_function)(events_t*, void*);
typedef void (*on_clean_function)(const struct engine_t, void*);

typedef struct engine_configuration_t {
	int width, height;
	float fps;

	graphic_api api;

	on_init_function on_init;
	on_resize_function on_resize;
	on_update_function on_update;
	on_event_function on_event;
	on_clean_function on_clean;

	void *data;
} engine_configuration_t;

typedef struct engine_t {
	float tpf;

	graphic_api api;
	engine_api_t graphic_api;

	on_init_function on_init;
	on_resize_function on_resize;
	on_update_function on_update;
	on_event_function on_event;
	on_clean_function on_clean;
	void *data;

	SDL_Window *window;
	SDL_Renderer *context;

	events_t events;
} engine_t;

define_result(engine_t);

result(engine_t) engine_new(engine_configuration_t config);
void engine_free(engine_t game);
void engine_main_loop(engine_t game);
