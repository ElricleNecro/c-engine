local bindings = {
	{ 'n', "<F1>", ":make! -j 8<cr>", { noremap = false, silent = false}},
	{ 'n', "<F2>", ":15split | terminal build/Debug/bin/ray<cr>", { noremap = false, silent = false}},
}

for _, bind in ipairs(bindings) do
	vim.api.nvim_set_keymap(bind[1], bind[2], bind[3], bind[4])
end

vim.g['dag_program_command'] = "build/Debug/bin/ray"

vim.o.errorformat = table.concat(
	{
		"../%f:%l:%c: %trreur: %m",
		"../%f:%l:%c: %tttention: %m",
		"../%f:%l:%c: %m"
	},
	','
)
