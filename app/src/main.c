#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>

#include <math.h>

#include <cglm/cglm.h>

#include "cengine/engine.h"
#include "cengine/buffer.h"
#include "cengine/vertex_array.h"
#include "cengine/uniform_buffer.h"
#include "cengine/stringview.h"
#include "cengine/shader.h"
#include "cengine/camera.h"
#include "cengine/math_utils.h"
#include "cengine/texture.h"

#include "cengine/shapes.h"

static void printf_mat4(FILE *out, mat4 matrix) {
	for(size_t idx=0; idx<4; idx++) {
		for(size_t jdx=0; jdx<4; jdx++)
			fprintf(out, "%f ", matrix[idx][jdx]);
		fprintf(out, "\n");
	}
}

typedef struct camera_controller_t {
	camera_t camera;

	vec3 velocity;
	vec3 direction;
} camera_controller_t;

bool update_camera(camera_controller_t *control, const events_t evt, const float elapsed_time) {
	if( evt.kPress[SDL_SCANCODE_W] )
		control->camera.position[1] += control->velocity[1] * elapsed_time;

	if( evt.kPress[SDL_SCANCODE_S] )
		control->camera.position[1] -= control->velocity[1] * elapsed_time;

	if( evt.kPress[SDL_SCANCODE_A] )
		control->camera.position[0] += control->velocity[0] * elapsed_time;

	if( evt.kPress[SDL_SCANCODE_D] )
		control->camera.position[0] += control->velocity[0] * elapsed_time;

	if( control->camera.kind == PERSPECTIVE ) {
		// Update yaw and pitch
	}

	control->camera = update_view_camera(control->camera);

	return false;
}

typedef struct vertices {
	float position[3];
	float color[4];

	float tex_coord[2];
	float tex_index;
	float tiling_factor;

	int entity_id;
} vertices;

typedef struct uniform_data {
	mat4 view_projection;
	mat4 transform;
} uniform_data_t;

typedef struct models {
	vertices *mesh;
	uint32_t *indices;

	texture_t tex;

	size_t npoints, ntriangles;

	vec3 position;
	vec3 rotation;
	vec3 scale;

	shader s_id;
	vertex_buffer vbo;
	index_buffer vio;
	vertex_array vao;
	uniform_buffer uniform;
} models;

typedef struct game_t {
	models *model;
	size_t nb_models;

	camera_controller_t camera;
} game_t;

models create_cube(vec3 position, vec3 rotation, vec3 scale, const float tex_index, const int entity_id, const char *path) {
	par_shapes_mesh *cube = par_shapes_create_cube();

	models data = {
		.mesh = calloc(cube->npoints, sizeof(vertices)),
		.indices = calloc(cube->ntriangles * 3, sizeof(uint32_t)),

		.npoints = cube->npoints,
		.ntriangles = cube->ntriangles,

		.position = {position[0], position[1], position[2]},
		.rotation = {rotation[0], rotation[1], rotation[2]},
		.scale = {scale[0], scale[1], scale[2]},
	};

	for(int idx = 0, point = 0; idx < cube->npoints; idx++,point += 3) {
		data.mesh[idx] = (vertices){
			.position = {cube->points[point + 0], cube->points[point + 1], cube->points[point + 2]},
			.color = {0.84f, 0.2f, 0.2f, 1.f},

			.tex_index = tex_index,
			.tiling_factor = 1.f,

			.entity_id = entity_id,
		};
	}

	memcpy(data.indices, cube->triangles, 3 * cube->ntriangles * sizeof(uint32_t));

	for(int idx=0; idx < cube->ntriangles * 3; idx += 6) {
		data.mesh[cube->triangles[idx + 0]].tex_coord[0] = 0.f;
		data.mesh[cube->triangles[idx + 0]].tex_coord[1] = 0.f;

		data.mesh[cube->triangles[idx + 1]].tex_coord[0] = 1.f;
		data.mesh[cube->triangles[idx + 1]].tex_coord[1] = 0.f;

		data.mesh[cube->triangles[idx + 2]].tex_coord[0] = 1.f;
		data.mesh[cube->triangles[idx + 2]].tex_coord[1] = 1.f;

		data.mesh[cube->triangles[idx + 4]].tex_coord[0] = 0.f;
		data.mesh[cube->triangles[idx + 4]].tex_coord[1] = 1.f;
	}

	if( path == NULL ) {
		data.tex = new_texture_empty(1, 1);
		static uint32_t white_pixel = 0xffffffff;
		set_data_texture(data.tex, &white_pixel, sizeof(uint32_t));
	}
	else
		data.tex = new_texture(path);

	PAR_FREE(cube);

	return data;
}

models create_cube_v2(vec3 position, vec3 rotation, vec3 scale, const float tex_index, const int entity_id, const char *path) {
	static const vertices __const_arr_vertices[] = {
		// Face 1:
		[0] = {
			.position = {0.f, 0.f, 0.f},
			.color = {0.84f, 0.2f, 0.2f, 1.f},

			.tex_coord = {0.f, 0.f},

			.tex_index = 0,
			.tiling_factor = 1.f,

			.entity_id = 0,
		},
		[1] = {
			.position = {0.f, 1.f, 0.f},
			.color = {0.84f, 0.2f, 0.2f, 1.f},

			.tex_coord = {0.f, 1.f},

			.tex_index = 0,
			.tiling_factor = 1.f,

			.entity_id = 0,
		},
		[2] = {
			.position = {1.f, 1.f, 0.f},
			.color = {0.84f, 0.2f, 0.2f, 1.f},

			.tex_coord = {1.f, 1.f},

			.tex_index = 0,
			.tiling_factor = 1.f,

			.entity_id = 0,
		},
		[3] = {
			.position = {1.f, 0.f, 0.f},
			.color = {0.84f, 0.2f, 0.2f, 1.f},

			.tex_coord = {1.f, 0.f},

			.tex_index = 0,
			.tiling_factor = 1.f,

			.entity_id = 0,
		},

		// Face 2
		[4] = {
			.position = {1.f, 0.f, 0.f},
			.color = {0.2f, 0.84f, 0.2f, 1.f},

			.tex_coord = {0.f, 0.f},

			.tex_index = 0,
			.tiling_factor = 1.f,

			.entity_id = 0,
		},
		[5] = {
			.position = {1.f, 1.f, 0.f},
			.color = {0.2f, 0.84f, 0.2f, 1.f},

			.tex_coord = {0.f, 1.f},

			.tex_index = 0,
			.tiling_factor = 1.f,

			.entity_id = 0,
		},
		[6] = {
			.position = {1.f, 1.f, 1.f},
			.color = {0.2f, 0.84f, 0.2f, 1.f},

			.tex_coord = {1.f, 1.f},

			.tex_index = 0,
			.tiling_factor = 1.f,

			.entity_id = 0,
		},
		[7] = {
			.position = {1.f, 0.f, 1.f},
			.color = {0.2f, 0.84f, 0.2f, 1.f},

			.tex_coord = {1.f, 0.f},

			.tex_index = 0,
			.tiling_factor = 1.f,

			.entity_id = 0,
		},

		// Face 3
		[8] = {
			.position = {1.f, 0.f, 1.f},
			.color = {0.84f, 0.84f, 0.2f, 1.f},

			.tex_coord = {0.f, 0.f},

			.tex_index = 0,
			.tiling_factor = 1.f,

			.entity_id = 0,
		},
		[9] = {
			.position = {1.f, 1.f, 1.f},
			.color = {0.84f, 0.84f, 0.2f, 1.f},

			.tex_coord = {0.f, 1.f},

			.tex_index = 0,
			.tiling_factor = 1.f,

			.entity_id = 0,
		},
		[10] = {
			.position = {0.f, 1.f, 1.f},
			.color = {0.84f, 0.84f, 0.2f, 1.f},

			.tex_coord = {1.f, 1.f},

			.tex_index = 0,
			.tiling_factor = 1.f,

			.entity_id = 0,
		},
		[11] = {
			.position = {0.f, 0.f, 1.f},
			.color = {0.84f, 0.84f, 0.2f, 1.f},

			.tex_coord = {1.f, 0.f},

			.tex_index = 0,
			.tiling_factor = 1.f,

			.entity_id = 0,
		},

		// Face 4
		[12] = {
			.position = {0.f, 0.f, 1.f},
			.color = {0.2f, 0.2f, 0.84f, 1.f},

			.tex_coord = {0.f, 0.f},

			.tex_index = 0,
			.tiling_factor = 1.f,

			.entity_id = 0,
		},
		[13] = {
			.position = {0.f, 1.f, 1.f},
			.color = {0.2f, 0.2f, 0.84f, 1.f},

			.tex_coord = {0.f, 1.f},

			.tex_index = 0,
			.tiling_factor = 1.f,

			.entity_id = 0,
		},
		[14] = {
			.position = {0.f, 1.f, 0.f},
			.color = {0.2f, 0.2f, 0.84f, 1.f},

			.tex_coord = {1.f, 1.f},

			.tex_index = 0,
			.tiling_factor = 1.f,

			.entity_id = 0,
		},
		[15] = {
			.position = {0.f, 0.f, 0.f},
			.color = {0.2f, 0.2f, 0.84f, 1.f},

			.tex_coord = {1.f, 0.f},

			.tex_index = 0,
			.tiling_factor = 1.f,

			.entity_id = 0,
		},

		// Face 5
		[16] = {
			.position = {0.f, 1.f, 0.f},
			.color = {0.2f, 0.2f, 0.2f, 1.f},

			.tex_coord = {0.f, 0.f},

			.tex_index = 0,
			.tiling_factor = 1.f,

			.entity_id = 0,
		},
		[17] = {
			.position = {1.f, 1.f, 0.f},
			.color = {0.2f, 0.2f, 0.2f, 1.f},

			.tex_coord = {0.f, 1.f},

			.tex_index = 0,
			.tiling_factor = 1.f,

			.entity_id = 0,
		},
		[18] = {
			.position = {1.f, 1.f, 1.f},
			.color = {0.2f, 0.2f, 0.2f, 1.f},

			.tex_coord = {1.f, 1.f},

			.tex_index = 0,
			.tiling_factor = 1.f,

			.entity_id = 0,
		},
		[19] = {
			.position = {0.f, 1.f, 1.f},
			.color = {0.2f, 0.2f, 0.2f, 1.f},

			.tex_coord = {1.f, 0.f},

			.tex_index = 0,
			.tiling_factor = 1.f,

			.entity_id = 0,
		},

		// Face 6
		[20] = {
			.position = {0.f, 0.f, 0.f},
			.color = {1.0f, 1.0f, 1.0f, 1.f},

			.tex_coord = {0.f, 0.f},

			.tex_index = 0,
			.tiling_factor = 1.f,

			.entity_id = 0,
		},
		[21] = {
			.position = {1.f, 0.f, 0.f},
			.color = {1.0f, 1.0f, 1.0f, 1.f},

			.tex_coord = {0.f, 1.f},

			.tex_index = 0,
			.tiling_factor = 1.f,

			.entity_id = 0,
		},
		[22] = {
			.position = {1.f, 0.f, 1.f},
			.color = {1.0f, 1.0f, 1.0f, 1.f},

			.tex_coord = {1.f, 1.f},

			.tex_index = 0,
			.tiling_factor = 1.f,

			.entity_id = 0,
		},
		[23] = {
			.position = {0.f, 0.f, 1.f},
			.color = {1.0f, 1.0f, 1.0f, 1.f},

			.tex_coord = {1.f, 0.f},

			.tex_index = 0,
			.tiling_factor = 1.f,

			.entity_id = 0,
		},
	};
	static const uint32_t __const_arr_indices[] = {
		 0,  1,  2,
		 2,  3,  0,

		 4,  5,  6,
		 6,  7,  4,

		 8,  9, 10,
		10, 11,  8,

		12, 13, 14,
		14, 15, 12,

		18, 17, 16,
		16, 19, 18,

		20, 21, 22,
		22, 23, 20,
	};
	static const uint32_t n_vertices = sizeof(__const_arr_vertices) / sizeof(__const_arr_vertices[0]);
	static const uint32_t n_triangles = sizeof(__const_arr_indices) / sizeof(__const_arr_indices[0]) / 3;

	models data = {
		.mesh = calloc(n_vertices, sizeof(vertices)),
		.indices = calloc(n_triangles * 3, sizeof(uint32_t)),

		.npoints = n_vertices,
		.ntriangles = n_triangles,

		.position = {position[0], position[1], position[2]},
		.rotation = {rotation[0], rotation[1], rotation[2]},
		.scale = {scale[0], scale[1], scale[2]},
	};

	memcpy(data.mesh, __const_arr_vertices, n_vertices * sizeof(__const_arr_vertices[0]));
	memcpy(data.indices, __const_arr_indices, n_triangles * sizeof(__const_arr_indices[0]) * 3);

	for(size_t idx=0; idx < n_vertices; idx++) {
		data.mesh[idx].tex_index = tex_index;
		data.mesh[idx].entity_id = entity_id;
	}

	if( path == NULL ) {
		data.tex = new_texture_empty(1, 1);
		static uint32_t white_pixel = 0xffffffff;
		set_data_texture(data.tex, &white_pixel, sizeof(uint32_t));
	}
	else
		data.tex = new_texture(path);

	return data;
}

void on_init(const engine_t game, void *data) {
	game_t *game_data = (game_t*)data;

	game_data->nb_models = 1;
	game_data->model = calloc(1, sizeof(models));

	layout_description desc[] = {
		{.name="position", .type=FLOAT3, .size=data_type_size(FLOAT3), .normalised=false},
		{.name="color", .type=FLOAT4, .size=data_type_size(FLOAT4), .normalised=false},

		{.name="tex_coord", .type=FLOAT2, .size=data_type_size(FLOAT2), .normalised=false},
		{.name="tex_index", .type=FLOAT, .size=data_type_size(FLOAT), .normalised=false},
		{.name="tiling_factor", .type=FLOAT, .size=data_type_size(FLOAT), .normalised=false},

		{.name="entity_id", .type=INT, .size=data_type_size(INT), .normalised=false},
	};

	for(size_t idx=0; idx < game_data->nb_models; idx++) {
		// game_data->model[idx] = create_cube((vec3){0.f, -1.f, -0.5f}, (vec3){0.6f, 0.f, 0.f}, (vec3){1.f, 2.f, 1.f}, idx, idx, NULL);
		game_data->model[idx] = create_cube_v2((vec3){0.f, 0.f, 0.f}, (vec3){0.f, 0.f, 0.f}, (vec3){1.f, 2.f, 1.f}, idx, idx, "assets/textures/checkerboard.png");

		game_data->model[idx].vao = new_vertex_array();
		game_data->model[idx].vbo = new_vertex_buffer_static(game_data->model[idx].mesh, game_data->model[idx].npoints * sizeof(vertices));

		game_data->model[idx].vao = set_vextex_buffer_vertex_array(
			game_data->model[idx].vao,
			game_data->model[idx].vbo,
			sizeof(desc) / sizeof(layout_description),
			desc
		);

		game_data->model[idx].vio = new_index_buffer_static(game_data->model[idx].indices, game_data->model[idx].ntriangles * 3);
		game_data->model[idx].vao = set_index_buffer_vertex_array(game_data->model[idx].vao, game_data->model[idx].vio);

		game_data->model[idx].s_id = new_shader_from_file("assets/shaders/textured.glsl");
		game_data->model[idx].uniform = new_uniform_buffer(sizeof(struct uniform_data), 0);
	}

	game_data->camera = (camera_controller_t){
		.camera=new_camera_perspective(
			radians(90.f),
			(float)game.events.width / (float)game.events.height,
			(vec3){2.f, 2.5f, 1.f},
			(vec3){0.f, 0.f, 0.f},
			(vec3){0.f, 1.f, 0.f}
		),
		.direction={0.f, 0.f, 0.f},
	};

	game.graphic_api.set_clear_color((vec4){0.f, 0.f, 0.f, 1.f});
}

void on_update(const engine_t game, const float elapsed, void *data) {
	game_t *game_data = (game_t*)data;

	game.graphic_api.clear();

	// update_camera(&game_data->camera, game.events, elapsed);

	uniform_data_t uniform;
	memcpy(uniform.view_projection, game_data->camera.camera.view_projection, sizeof(mat4));

	for(size_t idx=0; idx < game_data->nb_models; idx++) {
		game_data->model[idx].rotation[1] += elapsed * 1.01f;
		if( game_data->model[idx].rotation[1] >= 2.f * M_PI )
			game_data->model[idx].rotation[1] -= 2.f * M_PI;

		game_data->model[idx].rotation[2] -= elapsed * .51f;
		if( game_data->model[idx].rotation[2] >= 2.f * M_PI )
			game_data->model[idx].rotation[2] -= 2.f * M_PI;

		mat4 transform = {
			{1.f, 0.f, 0.f, 0.f},
			{0.f, 1.f, 0.f, 0.f},
			{0.f, 0.f, 1.f, 0.f},
			{0.f, 0.f, 0.f, 1.f},
		};

		glm_translate(transform, game_data->model[idx].position);
		for(size_t rdx=0; rdx<3; rdx++) {
			float axis[3] = {0.f};
			axis[rdx] = 1.f;
			glm_rotate(transform, game_data->model[idx].rotation[rdx], axis);
		}
		glm_scale(transform, game_data->model[idx].scale);

		memcpy(uniform.transform, transform, sizeof(mat4));

		set_data_uniform_buffer(game_data->model[idx].uniform, &uniform, sizeof(struct uniform_data), 0);
		bind_texture(game_data->model[idx].tex, idx);
		bind_vertex_array(game_data->model[idx].vao);
		bind_shader(game_data->model[idx].s_id);

		game.graphic_api.draw_indexed(game_data->model[idx].vao, 0);

		unbind_shader(game_data->model[idx].s_id);
		unbind_vertex_array(game_data->model[idx].vao);
	}
}

bool on_event(events_t *event, void *data) {
	game_t *game_data = (game_t*)data;

	if( event->kPress[SDL_SCANCODE_A] && event->kPress[SDL_SCANCODE_LCTRL] ) {
		event->quit = true;
		return true;
	}

	if( event->resized ) {
		game_data->camera.camera = update_aspect_ratio(game_data->camera.camera, (float)event->width / (float)event->height);
	}

	return false;
}

void on_clean([[maybe_unused]]const engine_t game, void *data) {
	game_t *game_data = (game_t*)data;

	for(size_t idx=0; idx < game_data->nb_models; idx++) {
		delete_index_buffer(game_data->model[idx].vio);
		delete_vertex_buffer(game_data->model[idx].vbo);
		delete_vertex_array(game_data->model[idx].vao);
		delete_shader(game_data->model[idx].s_id);

		free(game_data->model[idx].mesh);
		free(game_data->model[idx].indices);
	}

	free(game_data->model);
	free(game_data);
}

engine_configuration_t configure(void) {
#define WIDTH 1920
#define HEIGHT 1080
	engine_configuration_t config = {
		.width = WIDTH,
		.height = HEIGHT,
		.api = API_OPENGL,
		.on_init = on_init,
		.on_update = on_update,
		.on_event = on_event,
		.on_clean = on_clean,
		.data = calloc(1, sizeof(game_t)),
	};

	return config;
}
