CC=gcc

CFLAGS=-Wall -Wextra -std=c2x -Wswitch-enum -Wsuggest-attribute=pure -Wsuggest-attribute=const
DEFINES=-DPAR_SHAPES_T=uint32_t -D_XOPEN_SOURCE=700 -DCENGINE_IMPL -DDEBUG
INCLUDE=$(shell pkg-config --cflags sdl2 glew) -I ${PWD}/include -I ${PWD}/vendor/cglm/include
LFLAGS=-rdynamic
LINK=-lm $(shell pkg-config --libs sdl2 glew)

SRC=$(wildcard src/**.c)
OBJ=$(foreach F,$(SRC),$(subst .c,.o,$(subst src,build,$(F))))

.PHONY: all clean example engine

all: engine example

engine: build cengine

build/%.o: src/%.c
	$(CC) $(CFLAGS) $(DEFINES) $(INCLUDE) -c $< -o $@

build/shapes.o: src/shapes.c include/cengine/shapes.h
	$(CC) $(CFLAGS) $(DEFINES) $(INCLUDE) -c $< -o $@

cengine: $(OBJ)
	$(CC) $(LFLAGS) $^ -o $@ $(LINK)

build:
	@mkdir -p $@

clean:
	@rm -rf build
	@rm -f cengine
	@make -C app clean

example:
	make -C app
